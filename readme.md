##Custom Wolfblass Icon set

###Installation:

Install [fontcustom](https://github.com/FontCustom/fontcustom)

Requires Ruby 1.9.2+, FontForge with Python scripting

###Usage

Run `fontcustom compile /SVGs` in your terminal

You can either `watch` if adding in new SVGs or `compile`.

Update in sitecore project as required.
